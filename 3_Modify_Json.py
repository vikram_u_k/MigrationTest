#title           :ModifyJson
#description     :This script updates the user id's in project.json of the downloaded file as per SHS user sheet
#author          :HCL_Gitlab_migration_team_Raghavendra_a
#date            :20190917
#version         :2.2
#usage           :Modify_Json.py


import re, sys, os
import codecs
import ConfigParser

def chunks(file,size=1024):
    while 1:
        startat=fi.tell()
        print startat #file's object current position from the start
        fi.seek(size,1) #offset from current postion -->1
        data=fi.read()
        yield startat,fi.tell()-startat #doesnt store whole list in memory
        if not data:
            break


input_name=sys.argv[1]
tmp_name=sys.argv[1]+".bak"
#lookup="/home/hcl/Scripts/Migration/Dev/migration_scripts/SHSUserMapping.csv"

config = ConfigParser.ConfigParser()
config.read('Project_paths.cfg')
lookup=config.get('PATH','Lookup_path')

fi=open(input_name,'rb') 
fo=open(tmp_name,'w')
fl=open(lookup,'r')
data=""
for ele in chunks(fi):
    fi.seek(ele[0])#startat
    data=fi.read(ele[1])#endat
    for i in fl.readlines():
        m=re.search(r'(z00\w+)(;)(.+)',i,re.I)
        if(m):
            key1=m.group(1).rstrip()
            key2=m.group(3).rstrip()
            data = data.replace(key1, key2)
    fo.write(data)

fi.close()
fo.close()
fl.close()

os.remove(input_name)
os.rename(tmp_name,input_name)
