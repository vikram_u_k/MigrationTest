import gitlab, re, tarfile
import os, time, sys
import ConfigParser
import codecs,json

#####################################################################################
def Export(p):

	config = ConfigParser.ConfigParser()
	config.read('Project_paths.cfg')
	q=config.get('PATH','Download_path')
	dir_path=q+p.name.replace(" ","")
	exist=os.path.exists(dir_path)
	if not exist :
		os.mkdir(dir_path)
	else:
		print("path %s is already exist. \nExport failed" %(dir_path))
		exit(0)
		
	export = p.exports.create({})
    # Wait for the 'finished' status
	export.refresh()
	while export.export_status != 'finished':
		time.sleep(1)
		export.refresh()

	project_path=dir_path+"\\"+p.name.replace(" ","")+".tar.gz"
	if os.path.exists(project_path):
		os.remove(project_path)

	with open(project_path, 'wb') as f:
		export.download(streamed=True, action=f.write)
	time.sleep(10)	
	print("Project %s Successfully exported in path %s" %(sys.argv[1],dir_path))
	return dir_path,project_path

######################################################################################
def Extract(dir_path,project_path):
	if (project_path.endswith("tar.gz")):
		tar = tarfile.open(project_path)
		tar.extractall(path=dir_path)
		tar.close()
		print ("Extracted successfully")
	else:
		print ("Not a tar.gz file: '%s '" %(project_path))

#####################################################################################
def Project_members(Source,dir_path):
	project_json=dir_path+"\\"+"project.json"
	user_id=[]
	author_id=[]
	with open(project_json) as json_file:
		data = json.load(json_file)
		for p in data['project_members']:
			user_id.append(p['user_id'])
	print("project_members are - ")
	for i in user_id:
		print(i)
	with open(project_json) as json_file:
		for line in json_file:
			author_id=re.findall(r'(?<="author_id":)\s*\d+',line)

	author_id=list(set(author_id))
	author_id=map(int,author_id)
	if(len(author_id)>0):
		print("author ids are - ")
		for i in author_id:
			print(i)
	else:
		print("There is no author ID list")
	auth=list(set(author_id)-set(user_id))
	if(len(auth)==0):
		print("No extra author ID found")
		return project_json
	else:
		print("Remaining author IDs are -")
		for i in auth:
			print(i)
		print("Adding these authors information to project members")
		dict2=[]
		f = open(project_json, 'r')
		modified_json=project_json+".bak"
		f2 = open(modified_json, 'w')
		dic = json.load(f)
		source_id=dic['project_members'][0]['source_id']
		id_number=len(dic['project_members'])-1
		print("length is ", id_number)
		project_id=dic['project_members'][id_number]['id']
		dic2 ={}
		null=None
		for usr in auth:
			user = Source.users.get(usr)
			user1=str(user)
			attr = re.search(r'(\{.*)',user1)
			if(attr):
				dict1=eval(attr.group(1))
				project_id=project_id + 1
				dict2.append({"id":project_id,"access_level":10,"source_id":source_id,"source_type":"Project","user_id":usr,"notification_level":3,"created_at":dict1['created_at'],"updated_at":dict1['created_at'],"created_by_id":usr,"invite_email":null,"invite_token":null,"invite_accepted_at":null,"requested_at":null,"expires_at":null,"user":{"id":usr,"email":dict1['email'],"username":dict1['name']}})
		dic['project_members'].extend(dict2)
		json.dump(dic, f2)
		print("Below information is added in project members block")
		print(dict2)
		backup=project_json+"bak2"
		f.close()
		f2.close()
		os.rename(project_json,backup)
		os.rename(modified_json,project_json)
		os.rename(backup,modified_json)
		return project_json
		
#####################################################################################

def chunks(fi,size=1024):
    while 1:
        startat=fi.tell()
        #print startat #file's object current position from the start
        fi.seek(size,1) #offset from current postion -->1
        data=fi.read()
        yield startat,fi.tell()-startat #doesnt store whole list in memory
        if not data:
            break

#####################################################################################
def Modify_Json(project_json):
	input_name=project_json
	tmp_name=project_json+".bak"
	#lookup="/home/hcl/Scripts/Migration/Dev/migration_scripts/SHSUserMapping.csv"

	config = ConfigParser.ConfigParser()
	config.read('Project_paths.cfg')
	lookup=config.get('PATH','Lookup_path')

	fi=open(input_name,'rb') 
	fo=open(tmp_name,'w')
	fl=open(lookup,'r')
	data=""
	for ele in chunks(fi):
		fi.seek(ele[0])#startat
		data=fi.read(ele[1])#endat
		for i in fl.readlines():
			m=re.search(r'(z00\w+)(;)(.+)',i,re.I)
			if(m):
				key1=m.group(1).rstrip()
				key2=m.group(3).rstrip()
				data = data.replace(key1, key2)
		fo.write(data)
		time.sleep(15)

	fi.close()
	fo.close()
	time.sleep(5)
	fl.close()

	os.remove(input_name)
	os.rename(tmp_name,input_name)

#####################################################################################
def create_tar(project_name):
	config = ConfigParser.ConfigParser()
	config.read('Project_paths.cfg')
	q=config.get('PATH','tar_path')
	folder_name=""
	#os.chdir(sys.argv[1])
	tar_name=project_name+".tar.gz"
	old_tar_name="old_"+tar_name

	if(re.search(r'\.tar\.gz',tar_name)):
		folder_name=tar_name.replace(".tar.gz","")
	folder_name=q+folder_name
	oldname=folder_name+"/"+tar_name
	newname=folder_name+"/"+old_tar_name

	exist=os.path.exists(oldname)
	if exist:
		os.rename(oldname,newname)
	os.chdir(folder_name)
	p=os.listdir("./")
	print("Folder contains following files")
	for i in p:
		print(i)
	q=[]
	for i in p:
		if(re.search(r'(\.tar\.gz|\.csv)',i)):
			continue
		q.append(i)
	print("\ntar.gz file contains following files")
	for i in q:
		print(i)

	tar = tarfile.open(oldname, "w:gz")
	for name in q:
		newname=".\\"+name
		tar.add(newname)
	tar.close()
	time.sleep(15)
	
#####################################################################################
print("Export started")
print("It may take minutes to hours depend on size of the project and the netowrk bandwidth available")
try:
	Source = gitlab.Gitlab.from_config('SOURCE', ['python-gitlab.cfg'])
	p = Source.projects.get(sys.argv[1],statistics=True)
	project_name=p.name.replace(" ","")
	print("project name and path -  %s , %s \n" %(project_name,p.path_with_namespace))
except:
	print("Project not found")
	exit(0)
dir_path,project_path=Export(p)

print("Extracting exported %s.tar.gz file" %(project_name))
time.sleep(25)
Extract(dir_path,project_path)

print("Verifying all authors exist in project members")
time.sleep(25)
project_json=Project_members(Source,dir_path)

print("Modifying Json file by replacing source name to destination name")
Modify_Json(project_json)
print("Json file is successfully modified with new destination username")

print("Creating tar.gz file for directory %s" %(dir_path))
create_tar(project_name)

print("\nExport process completed successfully")
