#title           :Export
#description     :This script downloads the project from source in tar.gz format along with statistics in a CSV file 
#author          :HCL_Gitlab_migration_team_Raghavendra_a
#date            :20190917
#version         :2.2
#usage           :Export.py

import gitlab, re, tarfile
import os, time, sys
import ConfigParser

#Source.auth()
Source = gitlab.Gitlab.from_config('SOURCE', ['python-gitlab.cfg'])

p = Source.projects.get(sys.argv[1],statistics=True)
export = p.exports.create({})
    # Wait for the 'finished' status
export.refresh()
while export.export_status != 'finished':
    time.sleep(1)
    export.refresh()
# Download the result

config = ConfigParser.ConfigParser()
config.read('Project_paths.cfg')
q=config.get('PATH','Download_path')
dir_path=q+sys.argv[2]
exist=os.path.exists(dir_path)
if not exist :
    os.mkdir(dir_path)

project_path=dir_path+"\\"+sys.argv[2]+".tar.gz"
filename=sys.argv[2]+".tar.gz"
if os.path.exists(project_path):
    os.remove(project_path)

with open(project_path, 'wb') as f:
    export.download(streamed=True, action=f.write)

print("Successful export")

cwd = os.getcwd()
# Untarring dowloaded tar.gz file
if (filename.endswith("tar.gz")):
	os.chdir(dir_path)
	tar = tarfile.open(filename,"r:gz")
	tar.extractall()
	tar.close()
	print ("Extracted successfully")
else:
	print ("Not a tar.gz file: '%s '" %(project_path))
os.chdir(cwd)

# Creating CSV file which contains Issue counts, Wiki count, merge request count, commit count
csv_path=dir_path+"\\"+sys.argv[2]+".csv"
if os.path.exists(csv_path):
    os.remove(csv_path)

fh1=open(csv_path,"w")
fh1.write("Project_ID,Issue,Wiki,Merge,Commit,Size\n")
#Project = Source.projects.get(sys.argv[1])

#stat=Source.projects.get(id=sys.argv[1], statistics=True)
#print(stat)


stat1=str(p)
size=""
#    attrs = usr.customattributes.list()
attr = re.search(r'(\{.*)',stat1)
if(attr):
    dict1=eval(attr.group(1))
    size=dict1['statistics']['storage_size']


fh1.write(sys.argv[1]+","+str(len(p.issues.list()))+","+str(len(p.wikis.list()))+","+str(len(p.mergerequests.list()))+","+str(len(p.commits.list()))+","+str(size))


print("Project is saved in path - %s " %(dir_path))

