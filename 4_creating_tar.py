#title           :CreateTarFile
#description     :This script creates a tar file of the downloaded, modified project files .
#author          :HCL_Gitlab_migration_team_Raghavendra_a
#date            :20190917
#version         :2.2
#usage           :creating_tar.py

import tarfile
import os, re, sys
import ConfigParser


config = ConfigParser.ConfigParser()
config.read('Project_paths.cfg')
q=config.get('PATH','tar_path')
folder_name=""
#os.chdir(sys.argv[1])
tar_name=sys.argv[1]
old_tar_name="old_"+tar_name

if(re.search(r'\.tar\.gz',tar_name)):
	folder_name=tar_name.replace(".tar.gz","")
folder_name=q+folder_name
oldname=folder_name+"/"+sys.argv[1]
newname=folder_name+"/"+old_tar_name
os.rename(oldname,newname)
os.chdir(folder_name)
p=os.listdir("./")
print("Folder contains following files\n",p)
q=[]
for i in p:
        if(re.search(r'(\.tar\.gz|\.csv)',i)):
                continue
        q.append(i)
print("tar.gz file contains following files\n",q)

tar = tarfile.open(sys.argv[1], "w:gz")
for name in q:
	newname=".\\"+name
	tar.add(newname)
tar.close()
