#title           :ProjectMemberAuthorScan
#description     :This script finds the missing author id's in project_members section of the json and adds them to the json file
#author          :HCL_Gitlab_migration_team_Raghavendra_a
#date            :20190917
#version         :2.2
#usage           :projectmember_author_scan.py

import re, sys, os, time
import codecs,json
import gitlab
import ConfigParser

user_id=[]
author_id=[]
with open(sys.argv[1]) as json_file:
    data = json.load(json_file)
    for p in data['project_members']:
        user_id.append(p['user_id'])
print("project_members are - ",user_id)

with open(sys.argv[1]) as json_file:
    for line in json_file:
        author_id=re.findall(r'(?<="author_id":)\s*\d+',line)

author_id=list(set(author_id))
author_id=map(int,author_id)
print("author ids are - ",author_id)


auth=list(set(author_id)-set(user_id))
print("missing author ids are - ",auth)
if(len(auth)==0):
	exit(0)
else:
	value=raw_input("whether you want to add missing author ID's  to project_members?(Y/N)")
	if(value=="Y"):	
		Source = gitlab.Gitlab.from_config('SOURCE', ['python-gitlab.cfg'])
		dict2=[]
#dict2['project_members']=[]

#f=open('project_members.json','r+')
#f=open(sys.argv[1],'r')
		f = open(sys.argv[1], 'r')
		modified_json=sys.argv[1]+".bak"
		f2 = open(modified_json, 'w')
		dic = json.load(f)
		source_id=dic['project_members'][0]['source_id']
		id_number=len(dic['project_members'])-1
		print("length is ", id_number)
		project_id=dic['project_members'][id_number]['id']
		dic2 ={}
		null=None
		for usr in auth:
    			user = Source.users.get(usr)
    			user1=str(user)
#    attrs = usr.customattributes.list()
    			attr = re.search(r'(\{.*)',user1)
    			if(attr):
        			dict1=eval(attr.group(1))
        			project_id=project_id + 1
#        dict2.append({'email':dict1['email'],'username':dict1['username'],'name':dict1['name']})
        			dict2.append({"id":project_id,"access_level":10,"source_id":source_id,"source_type":"Project","user_id":usr,"notification_level":3,"created_at":dict1['created_at'],"updated_at":dict1['created_at'],"created_by_id":usr,"invite_email":null,"invite_token":null,"invite_accepted_at":null,"requested_at":null,"expires_at":null,"user":{"id":usr,"email":dict1['email'],"username":dict1['name']}})
#f2=open('project_members_2.json','w')
		dic['project_members'].extend(dict2)
		json.dump(dic, f2)
#print(dic['project_members'])
		print(dict2)
		backup=sys.argv[1]+"bak2"
		os.rename(sys.argv[1],backup)
		os.rename(modified_json,sys.argv[1])
		os.rename(backup,modified_json)
	else:
		exit(0)
